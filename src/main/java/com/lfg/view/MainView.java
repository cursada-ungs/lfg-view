package com.lfg.view;

import java.util.List;
import java.util.Observable;

import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.lfg.controller.MainController;

import LFG.Core.ObservableResult;
import main.InitCore;


public class MainView extends JFrame implements Observer {
	
	private static final long serialVersionUID = 1;
	
	private MainController maincontroller;
	
	private JPanel contentPanel;
	
	/* keywords */
	private JLabel keyword1;
	private JComboBox< String > options1;
	private JLabel keyword2;
	private JComboBox< String > options2;
	private JLabel keyword3;
	private JComboBox< String > options3;

	/* Drop finders */
	private JComboBox< String > finderOptions;
	
	/* Actions */
	private JButton btnSearch;
	
	/**
	 * 
	 */
	//public MainView( ObservableResult init ) {
	public MainView( InitCore init ) {
		// Subscribe
		// init.init().addObserver( this );

		//init.addObservers( this );
		init.attachObservers( this );
		this.setTitle( ".: Looking For Group :." );
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		this.setBounds( 0, 0, 600, 427);
		
		this.contentPanel = new JPanel();
		this.contentPanel.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
		this.contentPanel.setLayout( null );
		
		this.setContentPane( this.contentPanel );
		
		this.setLocationRelativeTo( null );
		
		// Add keyword menu (hard coded)
		this.keyword1 = new JLabel( "Palabra clave 1: " );
		this.keyword1.setBounds( 10, 10, 100, 27 );
		this.contentPanel.add( this.keyword1 );
		
		this.options1 = new JComboBox< String >();
		this.options1.setBounds( 200, 10, 50, 27 );
		this.options1.addItem( "1" );
		this.options1.addItem( "2" );
		this.contentPanel.add( this.options1 );
		
		this.keyword2 = new JLabel( "Palabra clave 2: " );
		this.keyword2.setBounds( 10, 40, 100, 27 );
		this.contentPanel.add( this.keyword2 );
		
		this.options2 = new JComboBox< String >();
		this.options2.setBounds( 200, 40, 50, 27 );
		this.options2.addItem( "A" );
		this.options2.addItem( "H" );
		this.contentPanel.add( this.options2 );
		
		this.keyword3 = new JLabel( "Palabra clave 3: " );
		this.keyword3.setBounds( 10, 70, 100, 27 );
		this.contentPanel.add( this.keyword3 );
		
		this.options3 = new JComboBox< String >();
		this.options3.setBounds( 200, 70, 50, 27 );
		this.options3.addItem( "R0" );
		this.options3.addItem( "R1" );
		this.contentPanel.add( this.options3 );
		
		// Finders
		this.finderOptions = new JComboBox< String >();
		this.finderOptions.setBounds( 10, 100, 100, 27 );
		this.contentPanel.add( this.finderOptions );
		
		// Actions
		this.btnSearch = new JButton();
		this.btnSearch.setText( "Search" );
		this.btnSearch.setBounds( 10, 200, 100, 27 );
		this.contentPanel.add( this.btnSearch );
		
		// Create controller
		// this.maincontroller = new MainController(  this, init.init() );
		this.maincontroller = new MainController( this, init );
	}

	@Override
	public void update( Observable o, Object arg ) {
		System.out.println( "MainView: update" );
	}
	
	public JComboBox getOptions1() {
		return this.options1;
	}

	public JComboBox getOptions2() {
		return this.options2;
	}
	
	public JComboBox getOptions3() {
		return this.options3;
	}

	public JButton getBtnSearch() {
		return this.btnSearch;
	}
	
	public JComboBox getFinderOptions() {
		return this.finderOptions;
	}
	
	public void showFinders( List<String> findersNames ) {
		findersNames.forEach( ( finder ) ->
			this.finderOptions.addItem( finder )
		);
	}
}