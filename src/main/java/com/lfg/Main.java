package com.lfg;

import java.io.File;
import java.io.IOException;

import javax.swing.UIManager;


import com.lfg.view.MainView;

import main.InitCore;


public class Main {

	public static void main(String[] args) throws Exception {
		
		try {
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		} catch( Exception e ) {
			e.printStackTrace();
		}
		
		String url = "plugins";
		
		// Verificación de que archivos esta tomando en la carpeta.
		/* 
		File[] files = new File( url ).listFiles();
		for( int i = 0; i < files.length; i++ ) {
			System.out.println( files[ i ].getName() );
		}
		*/
	
		InitCore init = new InitCore(url);
		MainView mainView = new MainView( init );		
	}
}
