package com.lfg.controller;

import com.lfg.view.MainView;

import LFG.Core.ObservableResult;
import LFG.Core.Post;
import interfaces.PostProvider;
import main.InitCore;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class MainController implements ActionListener, Observer {

	MainView mainView;
	ObservableResult observableResult;
	
	InitCore core;
	
	@SuppressWarnings("deprecation")
	public MainController(  MainView view, InitCore init) {
		this.core = init;
		this.mainView = view;
		view.setVisible( true );
		this.core.attachObservers( this );
				
		this.mainView.getBtnSearch().addActionListener( this );
		
		setFindersInView();
	}

	private void setFindersInView() {
		List< String > findersNames = new ArrayList< String >();
		
		this.core.postProviders.forEach( ( k, v ) -> findersNames.add( k ) );
		
		this.mainView.showFinders( findersNames );
	}

	@Override
	public void actionPerformed( ActionEvent event ) {
		if ( event.getSource() == this.mainView.getBtnSearch() ) {
			this.onSearch();
		}
	}

	private void onSearch() {
		
		String finderName = this.mainView.getFinderOptions().getSelectedItem().toString();
		
		List< String > keywords = new ArrayList<>();
		keywords.add( "Post" );

		keywords.add( this.mainView.getOptions1().getSelectedItem().toString() );
		keywords.add( this.mainView.getOptions2().getSelectedItem().toString() );
		keywords.add( this.mainView.getOptions3().getSelectedItem().toString() );
		
		System.out.println( keywords );

		Collection< Post > p = this.core.init( finderName, keywords );
		Iterator< Post > iterator = p.iterator();
		
		while( iterator.hasNext() ) {
			System.out.println( iterator.next().content );
		}
		
		//		Post p = (Post)(this.observableResult.find(keywords).toArray()[0]);
//		Collection< Post > p = this.observablesResults.get( finderName ).find( keywords );
		
//		Iterator< Post > iterator = p.iterator();
/*		
		while( iterator.hasNext() ) {
			System.out.println( iterator.next().content );
		}
*/		
	}

	@Override
	public void update( Observable o, Object arg ) {
		System.out.println( "MainController: update()" );
	}
}