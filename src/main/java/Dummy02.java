import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import LFG.Core.Post;
import interfaces.PostProvider;

public class Dummy02 implements PostProvider {

	@Override
	public Collection< Post > getPost( List< String > keywords ) {
		
		Collection< Post > c = new HashSet< Post >();
		
		c.add( new Post( "Dummy02 ClanWoW 1" ) );
		c.add( new Post( "Dummy02 ClanWoW 2" ) );
		c.add( new Post( "Dummy02 ClanWoW 3" ) );
		
		return c;
	}

}
