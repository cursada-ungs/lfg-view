import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import LFG.Core.Post;
import interfaces.PostProvider;

public class Dummy01 implements PostProvider {

	@Override
	public Collection<Post> getPost( List<String> keywords ) {
		
		Collection< Post > posts = new HashSet< Post >();
		
		posts.add( new Post( "Dummy01Clan 01" ) );
		posts.add( new Post( "Dummy01Clan 02" ) );
		
		return posts;
	}

}
